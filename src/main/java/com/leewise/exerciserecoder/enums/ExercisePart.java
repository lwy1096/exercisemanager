package com.leewise.exerciserecoder.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExercisePart {
    CHEST("상체"),
    BACK("등"),
    LEG("하체");

    private final String bodyPart;
}
