package com.leewise.exerciserecoder.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExerciseSet {
    ONE("1세트"),
    TWO("2세트"),
    THREE("3세트");
    private final String exerciseSet;
}
