package com.leewise.exerciserecoder.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExerciseLevel {

    BEGINNER("초수"),
    MIDDLE("중수"),
    HIGH("고수");

    private final String level;
}
