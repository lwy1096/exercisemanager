package com.leewise.exerciserecoder.entity;

import com.leewise.exerciserecoder.enums.ExerciseLevel;
import com.leewise.exerciserecoder.enums.Gender;
import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import com.leewise.exerciserecoder.model.MemberRequest;
import com.leewise.exerciserecoder.model.MemberWeightUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberInfo {
    @ApiModelProperty(value = "시퀀스값")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @ApiModelProperty(value = "회원명")
    @Column(nullable = false, length = 20)
    private String memberName;

    @ApiModelProperty(value = "회원 연락처")
    @Column(nullable = false)
    private String memberPhoneNum;

    @ApiModelProperty(value = "회원 성별")
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender memberGender;

    @ApiModelProperty(value = "회원신장")
    @Column(nullable = false)
    private Double memberHeight;

    @ApiModelProperty(value = "회원 몸무게")
    @Column(nullable = false)
    private Double memberWeight;

    @ApiModelProperty(value = "운동 레벨")
    @Column(nullable = false , length = 40)
    @Enumerated(EnumType.STRING)
    private ExerciseLevel exerciseLevel;

    @ApiModelProperty(value = "마지막 방문일")
    private LocalDate lastEnterDate;

    public void putMemberWeight(MemberWeightUpdateRequest request) {
        this.memberWeight = request.getMemberWeight();
    }

    public MemberInfo(MemberInfoBuilder builder) {
        this.memberName = builder.memberName;
        this.memberPhoneNum = builder.memberPhoneNum;
        this.memberGender = builder.memberGender;
        this.memberHeight = builder.memberHeight;
        this.memberWeight = builder.memberWeight;
        this.exerciseLevel = builder.exerciseLevel;
        this.lastEnterDate = builder.lastEnterDate;

    }

    public static class MemberInfoBuilder implements CommonModelBuilder<MemberInfo> {

        private final String memberName;
        private final String memberPhoneNum;
        private final Gender memberGender;
        private final Double memberHeight;
        private final Double memberWeight;
        private final ExerciseLevel exerciseLevel;
        private final LocalDate lastEnterDate;

        public MemberInfoBuilder(MemberRequest request) {
            this.memberName = request.getMemberName();
            this.memberPhoneNum = request.getMemberPhoneNum();
            this.memberGender = request.getMemberGender();
            this.memberWeight = request.getMemberWeight();
            this.memberHeight = request.getMemberHeight();
            this.exerciseLevel = request.getExerciseLevel();
            this.lastEnterDate = LocalDate.now();
        }
        @Override
        public MemberInfo build() {
            return new MemberInfo(this);
        }
    }


}
