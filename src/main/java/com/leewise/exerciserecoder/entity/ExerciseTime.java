package com.leewise.exerciserecoder.entity;

import com.leewise.exerciserecoder.enums.ExercisePart;
import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import com.leewise.exerciserecoder.model.ExerciseRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseTime {
    @ApiModelProperty(value = "시퀀스값")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "맴버 FK값")
    @JoinColumn(name = "MemberId",nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private MemberInfo memberInfo;

    @ApiModelProperty(value = "이넘 // 운동부위")
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ExercisePart exercisePart;

    @ApiModelProperty(value = "운동명")
    @Column(nullable = false, length = 40)
    private String exerciseName;


    @ApiModelProperty(value = "중랑 kg")
    private Integer weight;

    @ApiModelProperty(value = "운동 세트")

    private Integer exerciseSet;
    @ApiModelProperty(value = "세트당 운동")

    private Integer countExercise;

    @ApiModelProperty(value = "운동 시간")
    private Integer dateReserveHour;
    @ApiModelProperty(value = "운동 분")
    private Integer dateReserveMinuit;

    @ApiModelProperty(value = "운동 날짜")
    private LocalDate exerciseDate;
    @ApiModelProperty(value = "운동 시작 시간")
    private LocalTime startFitness;
    @ApiModelProperty(value = "운동 종료 시간")
    private LocalTime finishFitness;
    @ApiModelProperty(value = "목표 달성 여부")
    private  Boolean isGoal;
    @ApiModelProperty(value = "운동 종료 여부")
    private Boolean isFinished;

    public void putExerciseIsGoal() {
        this.isGoal = true;
        //setter로 불러올 수 없기 때문에 entity에서 작성 후 service에서 불러오게끔 작성.
    }

    public void putExerciseIsFinished() {
        this.isFinished = true;
        this.finishFitness = LocalTime.now();
    }
    public ExerciseTime(ExerciseTimeBuilder builder) {
        this.memberInfo = builder.memberInfo;
        this.exercisePart = builder.exercisePart;
        this.exerciseName = builder.exerciseName;
        this.weight = builder.weight;
        this.exerciseSet = builder.exerciseSet;
        this.countExercise = builder.countExercise;
        this.exerciseDate = builder.exerciseDate;
        this.dateReserveHour = builder.dateReserveHour;
        this.dateReserveMinuit = builder.dateReserveMinuit;
        this.startFitness = builder.startFitness;
        this.finishFitness = builder.finishFitness;
        this.isFinished = builder.isFinished;
        this.isGoal = builder.isGoal;

    }

    public static class ExerciseTimeBuilder implements CommonModelBuilder<ExerciseTime> {
        private  final MemberInfo memberInfo;
        private final ExercisePart exercisePart;
        private  final String exerciseName;
        private final Integer weight;
        private  final Integer exerciseSet;
        private final Integer countExercise;
        private final LocalDate exerciseDate;
        private final LocalTime startFitness;
        private final LocalTime finishFitness;
        private final Integer dateReserveHour;
        private final Integer dateReserveMinuit;
        private final Boolean isFinished;
        private final Boolean isGoal;
        public ExerciseTimeBuilder(ExerciseRequest request ,MemberInfo memberInfo) {
            this.memberInfo = memberInfo;
            this.exercisePart = request.getExercisePart();
            this.exerciseName = request.getExerciseName();
            this.weight = request.getWeight();
            this.exerciseSet = request.getExerciseSet();
            this.countExercise = request.getCountExercise();
            this.exerciseDate = request.getExerciseDate();
            this.startFitness = LocalTime.now();
            this.finishFitness = LocalTime.now();
            this.dateReserveHour = request.getDateReserveHour();
            this.dateReserveMinuit = request.getDateReserveMinuit();
            this.isFinished = false;
            this.isGoal = false;

        }

        @Override
        public ExerciseTime build() {
            return new ExerciseTime(this);
        }
    }
}
