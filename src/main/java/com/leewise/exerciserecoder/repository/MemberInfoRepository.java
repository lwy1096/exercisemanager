package com.leewise.exerciserecoder.repository;

import com.leewise.exerciserecoder.entity.MemberInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberInfoRepository extends JpaRepository<MemberInfo, Long> {
}
