package com.leewise.exerciserecoder.repository;

import com.leewise.exerciserecoder.entity.ExerciseTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;


public interface ExerciseTimeRepository extends JpaRepository<ExerciseTime, Long> {
    ExerciseTime findAllByMemberInfo_Id(long memberId);
    List<ExerciseTime> findAllByMemberInfo_IdAndExerciseDate(long memberId, LocalDate searchDate);


}
