package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.enums.ExerciseLevel;
import com.leewise.exerciserecoder.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {

    @ApiModelProperty(value = "회원명")
    @NotNull
    private String memberName;

    @ApiModelProperty(value = "회원 연락처")
    @NotNull
    private String memberPhoneNum;

    @ApiModelProperty(value = "회원 성별")
    @NotNull
    private Gender memberGender;

    @NotNull
    @ApiModelProperty(value = "회원 신장")
    private Double memberHeight;

    @NotNull
    @ApiModelProperty(value = "회원 몸무게")
    private Double memberWeight;

    @NotNull
    @ApiModelProperty(value = "운둥 레벨")
    private ExerciseLevel exerciseLevel;

}
