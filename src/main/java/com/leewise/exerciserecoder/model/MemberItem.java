package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.entity.MemberInfo;
import com.leewise.exerciserecoder.enums.ExerciseLevel;
import com.leewise.exerciserecoder.enums.Gender;
import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class MemberItem {

    @ApiModelProperty(value = "회원 성별")
    private Gender memberGender;
    @ApiModelProperty(value = "회원명")
    private String memberName;

    @ApiModelProperty(value = "회원 연락처")
    private String memberPhoneNum;
    @ApiModelProperty(value = "회원 신장")
    private String memberHeight;
    @ApiModelProperty(value = "회원 몸무게")
    private String memberWeight;
    @ApiModelProperty(value = "운동 레벨")
    private ExerciseLevel exerciseLevel;
    @ApiModelProperty(value = "회원 BMI")
    private String memberBMI;

    private MemberItem(MemberItemBuilder builder) {
        this.memberName = builder.memberName;
        this.memberGender = builder.memberGender;
        this.memberPhoneNum = builder.memberPhoneNum;
        this.memberHeight = builder.memberHeight;
        this.memberWeight = builder.memberWeight;
        this.exerciseLevel = builder.exerciseLevel;
        this.memberBMI = builder.memberBMI;

    }
    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final String memberName;
        private final Gender memberGender;
        private final String memberPhoneNum;
        private final String memberHeight;
        private final String memberWeight;
        private final ExerciseLevel exerciseLevel;
        private final String memberBMI;


        public MemberItemBuilder(MemberInfo memberInfo) {
            this.memberName = memberInfo.getMemberName();
            this.memberGender = memberInfo.getMemberGender();
            this.memberPhoneNum = memberInfo.getMemberPhoneNum();
            this.memberHeight = memberInfo.getMemberHeight() + "CM";
            this.memberWeight = memberInfo.getMemberWeight() + "KG";
            this.exerciseLevel = memberInfo.getExerciseLevel();
            Double heightDouble= (memberInfo.getMemberHeight()/100)*(memberInfo.getMemberHeight()/100);
            Double bmi = (memberInfo.getMemberWeight()/heightDouble);

            String bmiText = "";

            if(bmi<=18.5) {
                bmiText= "저체중";
            }else if(bmi<=22.9){
                bmiText= "정상";

            }else if(bmi<=24.9){
                bmiText="과체중";
            }else if(24.9<=bmi){
                bmiText="비만";
            }
            this.memberBMI = bmiText;
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }


}
