package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.enums.ExercisePart;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Setter
@Getter
public class ExerciseRequest {

    @ApiModelProperty(value = "운동 부위")
    @NotNull
    private ExercisePart exercisePart;
    @ApiModelProperty(value = "운동명")
    @NotNull
    private  String exerciseName;
    @NotNull
    @ApiModelProperty(value = "운동 중랑 kg")
    private Integer weight;
    @NotNull
    @ApiModelProperty(value = "운동 세트 ")
    private  Integer exerciseSet;
    @ApiModelProperty(value = "세트당 운동 횟수")
    @NotNull
    private Integer countExercise;

    @ApiModelProperty(value = "운동 날짜")
    @NotNull
    private LocalDate exerciseDate;

    @ApiModelProperty(value = "운동 시간")
    private Integer dateReserveHour;
    @ApiModelProperty(value = "운동 분")
    private Integer dateReserveMinuit;

}
