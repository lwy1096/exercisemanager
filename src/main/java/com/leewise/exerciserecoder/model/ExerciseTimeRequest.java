package com.leewise.exerciserecoder.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalTime;

@Getter
@Setter
public class ExerciseTimeRequest {
    @ApiModelProperty(value = "운동 시간")
    private Integer dateReserveHour;
    @ApiModelProperty(value = "운동 분")
    private Integer dateReserveMinuit;

}
