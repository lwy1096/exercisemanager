package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.entity.ExerciseTime;
import com.leewise.exerciserecoder.enums.ExercisePart;
import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseItem {
    @ApiModelProperty(value = "시퀀스값")
    private Long id;
    @ApiModelProperty(value = "회원명")
    private String memberName;
    @ApiModelProperty(value = "회원 연락처")
    private String memberPhoneNum;
    @ApiModelProperty(value = "운동 부위")
    private ExercisePart exercisePart;
    @ApiModelProperty(value = "운동명")
    private String exerciseName;

    @ApiModelProperty(value = "운동 세트")
    private Integer exerciseSet;
    
    @ApiModelProperty(value = "중량&세트당 운동 횟수")
    private String weightAndCountExercise;
    @ApiModelProperty(value = "운동 날짜")
    private LocalDate exerciseDate;

    @ApiModelProperty(value = "운동 시간")
    private String totalExerciseTime;

    private ExerciseItem(ExerciseItemBuilder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.memberPhoneNum = builder.memberPhoneNum;
        this.exercisePart = builder.exercisePart;
        this.exerciseName = builder.exerciseName;
        this.weightAndCountExercise = builder.weightAndCountExercise;
        this.exerciseDate = builder.exerciseDate;
        this.exerciseSet = builder.exerciseSet;
        this.totalExerciseTime = builder.totalExerciseTime;

    }
    public static class ExerciseItemBuilder implements CommonModelBuilder<ExerciseItem> {
        private final Long id;
        private final String memberName;
        private final String memberPhoneNum;
        private final ExercisePart exercisePart;
        private final String exerciseName;
        private final Integer exerciseSet;
        private final LocalDate exerciseDate;
        private final String weightAndCountExercise;
        private final String totalExerciseTime;

        public ExerciseItemBuilder(ExerciseTime exerciseTime) {
            this.id = exerciseTime.getId();
            this.memberName = exerciseTime.getMemberInfo().getMemberName();
            this.memberPhoneNum = exerciseTime.getMemberInfo().getMemberPhoneNum();
            this.exercisePart = exerciseTime.getExercisePart();
            this.exerciseName = exerciseTime.getExerciseName();
            this.weightAndCountExercise = exerciseTime.getWeight() +"kg" +"/"+exerciseTime.getCountExercise() +"개";
            this.totalExerciseTime = exerciseTime.getDateReserveHour() + "시간"+ " / " + exerciseTime.getDateReserveMinuit() + "분";
            this.exerciseSet = exerciseTime.getExerciseSet();
            this.exerciseDate = exerciseTime.getExerciseDate();

        }
        @Override
        public ExerciseItem build() {
            return new ExerciseItem(this);
        }
    }
}
