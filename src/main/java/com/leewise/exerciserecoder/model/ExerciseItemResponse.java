package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)

public class ExerciseItemResponse {
    @ApiModelProperty(value = "전체 운동 시간")
    private String totalRunningTimeName;
    @ApiModelProperty(value = "전체 운동 파트")

    private String todayParts;
    @ApiModelProperty(value = "회원 운동 정보 리스트")

    private List<MemberFitnessItem> memberFitnessItems;


    private ExerciseItemResponse(ExerciseItemResponseBuilder builder) {
        this.totalRunningTimeName = builder.totalRunningTimeName;
        this.todayParts = builder.todayParts;
        this.memberFitnessItems = builder.memberFitnessItems;

    }

    public static class ExerciseItemResponseBuilder implements CommonModelBuilder<ExerciseItemResponse> {
        private final String totalRunningTimeName;
        private final String todayParts;
        private final List<MemberFitnessItem> memberFitnessItems;


        public ExerciseItemResponseBuilder(String totalRunningTimeName, String todayParts, List<MemberFitnessItem> exerciseItems) {
            this.totalRunningTimeName = totalRunningTimeName;
            this.todayParts = todayParts;
            this.memberFitnessItems = exerciseItems;
        }
        @Override
        public ExerciseItemResponse build() {
            return new ExerciseItemResponse(this);
        }
    }

}
