package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberInfoResponse {
    @ApiModelProperty(value = "총 회원 수")
    private Long totalUserCount;

    @ApiModelProperty(value = "회원 평균 신장")
    private Double averageHeight;

    @ApiModelProperty(value = "회원 평균 몸무게")
    private Double averageWeight;
    @ApiModelProperty(value = "초보 회원 수")
    private Long beginnerUserCount;

    @ApiModelProperty(value = "중수 회원 수")
    private Long intermediateUserCount;

    @ApiModelProperty(value = "고수 회원 수")
    private Long seniorUserCount;

    private MemberInfoResponse(MemberInfoResponseBuilder builder) {
        this.totalUserCount = builder.totalUserCount;
        this.averageHeight = builder.averageHeight;
        this.averageWeight = builder.averageWeight;
        this.beginnerUserCount = builder.beginnerUserCount;
        this.intermediateUserCount = builder.intermediateUserCount;
        this.seniorUserCount = builder.seniorUserCount;
    }

    public static class MemberInfoResponseBuilder implements CommonModelBuilder<MemberInfoResponse> {
        private final Long totalUserCount;
        private final Double averageHeight;
        private final Double averageWeight;
        private final Long beginnerUserCount;
        private final Long intermediateUserCount;
        private final Long seniorUserCount;

        public MemberInfoResponseBuilder(
                Long totalUserCount,
                Double averageHeight,
                Double averageWeight,
                Long beginnerUserCount,
                Long intermediateUserCount,
                Long seniorUserCount
        ) {
            this.totalUserCount = totalUserCount;
            this.averageHeight = averageHeight;
            this.averageWeight = averageWeight;
            this.beginnerUserCount = beginnerUserCount;
            this.intermediateUserCount = intermediateUserCount;
            this.seniorUserCount = seniorUserCount;
        }


        @Override
        public MemberInfoResponse build() {
            return new MemberInfoResponse(this);
        }
    }
}

