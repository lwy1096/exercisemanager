package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.entity.ExerciseTime;
import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberFitnessItem {

    @ApiModelProperty(value = "시퀀스값")
    private Long id;
    @ApiModelProperty(value = "회원명")
    private String memberName;
    @ApiModelProperty(value = "회원 연락처")
    private String memberPhoneNum;
    @ApiModelProperty(value = "운동 파트명")
    // 운동 어디 했는지
    private String exercisePartName;

    @ApiModelProperty(value = "운동 날짜")
    private LocalDate exerciseDate;

    @ApiModelProperty(value = "운동 타임(분)")
    // 총 몇분 했는지
    private Long runningTime;

    @ApiModelProperty(value = "운동 중량kg")
    // 중량 몇으로 했는지
    private Integer averageWeight;

    @ApiModelProperty(value = " 총 운동 횟수")
    // 총 몇회 했는지
    private Integer totalCount;

    @ApiModelProperty(value = " 운동 종료 여부")
    private Boolean isFinished;
    @ApiModelProperty(value = " 운동 목표 달성 여부")
    private  Boolean isGoal;





    private MemberFitnessItem(MyFitnessItemBuilder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.memberPhoneNum = builder.memberPhoneNum;
        this.exercisePartName = builder.exercisePartName;
        this.exerciseDate = builder.exerciseDate;
        this.runningTime = builder.runningTime;
        this.averageWeight = builder.averageWeight;
        this.totalCount = builder.totalCount;
        this.isFinished = builder.isFinished;
        this.isGoal = builder.isGoal;


    }

    public static class MyFitnessItemBuilder implements CommonModelBuilder<MemberFitnessItem> {
        private final Long id;
        private final String memberName;
        private final String memberPhoneNum;
        private final LocalDate exerciseDate;
        private final String exercisePartName;
        private final Long runningTime;
        private final Integer averageWeight;
        private final Integer totalCount;
        private final Boolean isFinished;
        private final Boolean isGoal;



        public MyFitnessItemBuilder( ExerciseTime exerciseTime ,String exercisePartName, Long runningTime, Integer averageWeight, Integer totalCount,Boolean isFinished, Boolean isGoal) {
            this.id = exerciseTime.getId();
            this.memberName = exerciseTime.getMemberInfo().getMemberName();
            this.memberPhoneNum = exerciseTime.getMemberInfo().getMemberPhoneNum();
            this.exerciseDate = exerciseTime.getExerciseDate();
            this.exercisePartName = exercisePartName;
            this.runningTime = runningTime;
            this.averageWeight = averageWeight;
            this.totalCount = totalCount;
            this.isFinished = isFinished;
            this.isGoal = isGoal;

        }

        @Override
        public MemberFitnessItem build() {
            return new MemberFitnessItem(this);
        }
    }
}
