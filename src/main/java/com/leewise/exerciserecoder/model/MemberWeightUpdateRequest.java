package com.leewise.exerciserecoder.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberWeightUpdateRequest {
    private Double memberWeight;

}
