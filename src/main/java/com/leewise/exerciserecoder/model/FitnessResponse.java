package com.leewise.exerciserecoder.model;

import com.leewise.exerciserecoder.entity.ExerciseTime;
import com.leewise.exerciserecoder.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FitnessResponse {
    @ApiModelProperty(value = "시퀀스값")
    private Long id;
    @ApiModelProperty(value = "회원명")
    private String memberName;
    @ApiModelProperty(value = "회원 연락처")
    private String memberPhoneNum;
    @ApiModelProperty(value = "전체 운동 시간")
    private String totalRunningTimeName;

    @ApiModelProperty(value = "운동 부위")
    private String todayParts;

    @ApiModelProperty(value = "운동명")
    private String exerciseItems;

    private FitnessResponse(FitnessResponseBuilder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.memberPhoneNum = builder.memberPhoneNum;
        this.totalRunningTimeName = builder.totalRunningTimeName;
        this.todayParts = builder.todayParts;
        this.exerciseItems = builder.exerciseItems;
    }

    public static class FitnessResponseBuilder implements CommonModelBuilder<FitnessResponse> {

        private final Long id;
        private final String memberName;
        private final String memberPhoneNum;
        private final String totalRunningTimeName;
        private final String todayParts;
        private final String exerciseItems;

        public FitnessResponseBuilder(ExerciseTime exerciseTime) {
            this.id = exerciseTime.getMemberInfo().getId();
            this.memberName = exerciseTime.getMemberInfo().getMemberName();
            this.memberPhoneNum = exerciseTime.getMemberInfo().getMemberPhoneNum();
            this.totalRunningTimeName = exerciseTime.getDateReserveHour() +"시간" +exerciseTime.getDateReserveMinuit() + "분";
            this.todayParts = exerciseTime.getExercisePart().getBodyPart();
            this.exerciseItems = exerciseTime.getExerciseName();
        }

        @Override
        public FitnessResponse build() {
            return new FitnessResponse(this);
        }
    }
}

