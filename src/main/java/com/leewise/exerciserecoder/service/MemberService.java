package com.leewise.exerciserecoder.service;

import com.leewise.exerciserecoder.entity.MemberInfo;
import com.leewise.exerciserecoder.exception.CMissingDataException;
import com.leewise.exerciserecoder.model.ListResult;
import com.leewise.exerciserecoder.model.MemberItem;
import com.leewise.exerciserecoder.model.MemberRequest;
import com.leewise.exerciserecoder.model.MemberWeightUpdateRequest;
import com.leewise.exerciserecoder.repository.MemberInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberInfoRepository memberInfoRepository;
    public void setMember(MemberRequest request) {
        MemberInfo originData = new MemberInfo.MemberInfoBuilder(request).build();
        memberInfoRepository.save(originData);
    }
    public ListResult<MemberItem> getMember() {
        List<MemberInfo> originData = memberInfoRepository.findAll();
        List<MemberItem> result = new LinkedList<>();
        originData.forEach(item -> result.add(new MemberItem.MemberItemBuilder(item).build()));

      return ListConvertService.settingResult(result);
    }
    public void putMemberWeight(long id, MemberWeightUpdateRequest request) {
        MemberInfo originData = memberInfoRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putMemberWeight(request);
        memberInfoRepository.save(originData);
    }
    public MemberInfo getData(long id) {
        return memberInfoRepository.findById(id).orElseThrow();
    }

}
