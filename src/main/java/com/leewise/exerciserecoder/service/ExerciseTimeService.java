package com.leewise.exerciserecoder.service;

import com.leewise.exerciserecoder.entity.ExerciseTime;
import com.leewise.exerciserecoder.entity.MemberInfo;
import com.leewise.exerciserecoder.enums.ExerciseLevel;
import com.leewise.exerciserecoder.exception.CMissingDataException;
import com.leewise.exerciserecoder.model.*;
import com.leewise.exerciserecoder.repository.ExerciseTimeRepository;
import com.leewise.exerciserecoder.repository.MemberInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor

public class ExerciseTimeService {
    private final MemberInfoRepository memberInfoRepository;
    private final ExerciseTimeRepository exerciseTimeRepository;
    public void setExerciseTime(MemberInfo memberInfo, ExerciseRequest request) {
        ExerciseTime originData = new ExerciseTime.ExerciseTimeBuilder(request,memberInfo).build();
        exerciseTimeRepository.save(originData);
    }

    public MemberInfoResponse getStaticsByMember() {
        List<MemberInfo> originList  = memberInfoRepository.findAll();
        long totalUserCount = originList.size(); // 총 회원 수
        double totalHeight = 0; // 총 키의 합산
        double totalWeight = 0; // 총 몸무게의 합산
        long beginnerUserCount = 0; // 초급자 수
        long intermediateUserCount = 0; // 중급자 수
        long seniorUserCount = 0; // 고급자 수

        for (MemberInfo item : originList) {
            totalHeight += item.getMemberHeight();
            totalWeight += item.getMemberWeight();

            if (item.getExerciseLevel().equals(ExerciseLevel.BEGINNER)) {
                beginnerUserCount += 1;
            } else if (item.getExerciseLevel().equals(ExerciseLevel.MIDDLE)) {
                intermediateUserCount += 1;
            } else if (item.getExerciseLevel().equals(ExerciseLevel.HIGH)) {
                seniorUserCount += 1;
            }
        }
        return new MemberInfoResponse.MemberInfoResponseBuilder(
                totalUserCount,
                totalHeight / totalUserCount,
                totalWeight / totalUserCount,
                beginnerUserCount,
                intermediateUserCount,
                seniorUserCount
        ).build();

    }
// 회원리스트 뽑기
    public ListResult<ExerciseItem> getMemberExercise() {
        List<ExerciseTime> originData = exerciseTimeRepository.findAll();
        List<ExerciseItem> result = new LinkedList<>();
        originData.forEach(item -> result.add(new ExerciseItem.ExerciseItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public FitnessResponse getMemberStatisticsById(long memberId) {
        ExerciseTime originData = exerciseTimeRepository.findAllByMemberInfo_Id(memberId);
        return new FitnessResponse.FitnessResponseBuilder(originData).build();
    }
    public FitnessResponse getFitnessStatisticById(long id) {
        ExerciseTime originData = exerciseTimeRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new FitnessResponse.FitnessResponseBuilder(originData).build();
    }

    public ExerciseItemResponse getStatisticsByMyFitness(long memberId) {
        List<ExerciseTime> originList = exerciseTimeRepository.findAllByMemberInfo_IdAndExerciseDate(memberId, LocalDate.now());// 오늘날짜로 특정 회원이 운동 한 리스트를 가져와라

        long totalRunningTime = 0; // 총 운동시간(분) 담을 박스 생성, 기본값은 0 (리스트가 없으면/ 운동 안했으면 몇분?? 0분)
        List<String> partNames = new LinkedList<>(); // 어느 부위들을 운동했는지 이름들을 담아 둘 리스트 생성
        List<MemberFitnessItem> memberFitnessItems = new LinkedList<>(); // 오늘 한 운동의 아이템들을 담아 둘 리스트 생성

        for (ExerciseTime item : originList) { // 원본 가져온거 하나씩 던져주면서 무언가를 반복 실행 한다.
            // ------------ 여기서부터 반복 시작
            Duration duration = Duration.between(item.getStartFitness(), item.getFinishFitness()); // 운동 시작시간과 운동 종료시간 간격을 구한다.
            long second = duration.getSeconds(); // 위에서 구한 간격을 초로 환산한다.
            totalRunningTime += second / 60; // 총 운동시간(분)에 계속 더해져야 하니까. 운동한 초를 분으로 바꿔서 totalRunningTime에 더한다.

            partNames.add(item.getExercisePart().getBodyPart()); // partNames 리스트에 중복되더라도 일단 운동 부위명을 집어넣는다.

            // 새로운 MemberFitnessItem 모양 그릇을 만든다. 근데 빌더로 만들었으니까 값이 다 채워지면서 새로운 그릇이 만들어진다.
            MemberFitnessItem addItem = new MemberFitnessItem.MyFitnessItemBuilder(
                    item,
                    item.getExercisePart().name(),
                    second/60,
                    item.getWeight(),
                    item.getExerciseSet()*item.getCountExercise(),
                    item.getIsFinished(),
                    item.getIsGoal()

            ).build();


            memberFitnessItems.add(addItem); // 위에서 만든 addItem을 오늘 한 운동의 아이템들을 담아 둘 리스트에 추가한다.
            // ------------- 여기까지 반복..(반복의 끝)
        }

        Set<String> partNamesResult = new HashSet<>(partNames); // List<String> partNames는 중복된 값이 들어가도되는 리스트였는데 우리는 지금 중복값이 들어가면 안되어서 Set으로 변경해서 중복값을 제거해버린다. (Set은 리스트랑 같은 애인데 다른거라면 중복된 값이 들어가지 못하는 거 차이일 뿐이다.)
        String partNamesResultName = String.join("/", partNamesResult); // 중복된 값이 제거된 리스트 partNamesResult를 "/"로 이어진 문자열로 바꾼다. 예) ["몸통","하체"] -> "몸통/하체"

        // 마지막으로 ExerciseItemResponse 모양의 그릇에 넣을 값이 다 준비되었기 때문에 ExerciseItemResponse안에 빌더한테 값을 다 넘겨주면서 ExerciseItemResponse를 만들어달라고 한다.
        // 왜? getStatisticsByMyFitness라는 메서드(사람)은 ExerciseItemResponse라는 모양을 주기로 약속 했으니까.
        return new ExerciseItemResponse.ExerciseItemResponseBuilder(totalRunningTime + "분", partNamesResultName, memberFitnessItems).build();
    }
    public void putExerciseIsGoal(long id) {
        ExerciseTime originData = exerciseTimeRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putExerciseIsGoal();
        exerciseTimeRepository.save(originData);
    }
    public void putExerciseIsFinished(long id) {
        ExerciseTime originData = exerciseTimeRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putExerciseIsFinished();
        exerciseTimeRepository.save(originData);
    }
    public void delExercise(long id) {
        exerciseTimeRepository.deleteById(id);
    }
}
