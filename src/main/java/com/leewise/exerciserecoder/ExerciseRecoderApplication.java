package com.leewise.exerciserecoder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExerciseRecoderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExerciseRecoderApplication.class, args);
    }

}
