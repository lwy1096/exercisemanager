package com.leewise.exerciserecoder.controller;


import com.leewise.exerciserecoder.model.*;
import com.leewise.exerciserecoder.service.MemberService;
import com.leewise.exerciserecoder.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags="회원등록 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/Member")
public class MemberController {
    private final MemberService memberService;
    @ApiOperation(value = "회원 등록")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "회원조회")
    @GetMapping("/all")
    public ListResult<MemberItem> getMember() {
        return ResponseService.getListResult(memberService.getMember(),true);
    }

    @ApiOperation(value = "회원 몸무게 변경")
    @PutMapping("/change-weight/{id}")
    public CommonResult putMemberWeight(@PathVariable long id, @RequestBody @Valid MemberWeightUpdateRequest request) {
        memberService.putMemberWeight(id,request);
        return ResponseService.getSuccessResult();
    }

}
