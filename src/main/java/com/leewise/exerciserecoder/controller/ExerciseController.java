package com.leewise.exerciserecoder.controller;

import com.leewise.exerciserecoder.entity.ExerciseTime;
import com.leewise.exerciserecoder.entity.MemberInfo;
import com.leewise.exerciserecoder.model.*;
import com.leewise.exerciserecoder.service.ExerciseTimeService;
import com.leewise.exerciserecoder.service.ListConvertService;
import com.leewise.exerciserecoder.service.MemberService;
import com.leewise.exerciserecoder.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Api(tags="회원 운동 기록 등록 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/exercise")
public class ExerciseController {
    private final MemberService memberService;
    private final ExerciseTimeService exerciseTimeService;
    @ApiOperation(value = "회원 운동 기록 등록")
    @PostMapping("/member/exercise/{id}")
    public CommonResult setExercise( @PathVariable long id, @RequestBody @Valid ExerciseRequest request ){
        MemberInfo memberInfo = memberService.getData(id);
        exerciseTimeService.setExerciseTime(memberInfo,request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 기록 조회")
    @GetMapping("/member/all")
    public ListResult<ExerciseItem> getMemberAll() {
        return ResponseService.getListResult(exerciseTimeService.getMemberExercise(),true);
    }

    @ApiOperation(value = "회원 기록 조회 /(평균값)")
    @GetMapping("/member")
    public SingleResult<MemberInfoResponse> getExerciseInfo() {
        return ResponseService.getSingleResult(exerciseTimeService.getStaticsByMember());

    }
    @ApiOperation(value = "회원조회(회원아이디기준)")
    @GetMapping("/member/{memberId}")
    public SingleResult<FitnessResponse> getMemberStatisticsById(@PathVariable long memberId) {
        return ResponseService.getSingleResult(exerciseTimeService.getMemberStatisticsById(memberId));
    }

    @ApiOperation(value = "회원조회(아이디기준)")
    @GetMapping("/exercise/member/{id}")
    public SingleResult<FitnessResponse> getFitnessStatisticById (@PathVariable long id) {
        return ResponseService.getSingleResult(exerciseTimeService.getFitnessStatisticById(id));
    }


    @ApiOperation(value = "회원조회(오늘 날짜 기준)")
    @GetMapping("/fitness/{memberId}")
    public SingleResult<ExerciseItemResponse> getStaticsByFitness(@PathVariable long memberId) {
        return ResponseService.getSingleResult(exerciseTimeService.getStatisticsByMyFitness(memberId));
    }
    @ApiOperation(value = "회원 목표달성 여부 변경")
    @PutMapping("/goal/{id}")
    public CommonResult putExerciseIsGoal(@PathVariable long id ) {
        exerciseTimeService.putExerciseIsGoal(id);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "회원 운동 목표 달성 여부 변경")
    @PutMapping("/finished/{id}")
    public CommonResult putExerciseIsFinished(@PathVariable long id) {
        exerciseTimeService.putExerciseIsFinished(id);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value ="운동 기록 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delExercise(@PathVariable long id) {
        exerciseTimeService.delExercise(id);
        return ResponseService.getSuccessResult();
    }
}
