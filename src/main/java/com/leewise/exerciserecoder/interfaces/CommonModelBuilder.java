package com.leewise.exerciserecoder.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

